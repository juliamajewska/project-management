package com.ug.projekt.service.project;

import com.ug.projekt.dto.AddProjectDto;
import com.ug.projekt.dto.ProjectDto;
import com.ug.projekt.dto.SearchOptionsDto;
import com.ug.projekt.dto.SortOptionDto;

import java.util.List;

public interface ProjectService {
    List<ProjectDto> getAllProjects();
    ProjectDto getProjectById(Long id);
    void deleteProjectById(Long id);

    ProjectDto addProject(AddProjectDto addProjectDto);
    List<ProjectDto> searchProject(SearchOptionsDto searchOptionsDto, Integer page, SortOptionDto sortOptionDto);

}
