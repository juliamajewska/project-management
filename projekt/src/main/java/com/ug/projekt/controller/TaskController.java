package com.ug.projekt.controller;

import com.ug.projekt.dto.DeveloperAssignDto;
import com.ug.projekt.dto.TasksDto;
import com.ug.projekt.service.task.TaskService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/task")
public class TaskController {

    private final TaskService tasksService;

    public TaskController(TaskService taskService) {
        this.tasksService = taskService;
    }

    @GetMapping("")
    ResponseEntity<List<TasksDto>> getTasks() {
        return new ResponseEntity<>(tasksService.getAllTasks(), HttpStatus.OK);
    }

    @PostMapping("/{taskId}/assignDeveloper")
    ResponseEntity<TasksDto> assignDev(@PathVariable("taskId") Long taskId, @RequestBody DeveloperAssignDto developerAssignDto) {
        TasksDto task = tasksService.assignDeveloperToTask(developerAssignDto, taskId);
        return new ResponseEntity<>(task, HttpStatus.OK);
    }
}
