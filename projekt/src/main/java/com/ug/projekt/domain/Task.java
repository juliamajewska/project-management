package com.ug.projekt.domain;

import jakarta.persistence.*;

import java.time.LocalDate;
import java.time.Month;

@Entity
@Table
public class Task {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private String description;

    private LocalDate start_date;

    private LocalDate estimated_end_date;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "project_id")
    private Project project;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "developer_id")
    private Developer developer;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "label_id")
    private Label label;

    public Task() {}

    public Task(String name, String description, Project project, Developer developer, Label label) {
        this.name = name;
        this.description = description;
        this.project = project;
        this.developer = developer;
        this.label = label;
        this.start_date = LocalDate.of(2024, Month.FEBRUARY, 1);
        this.estimated_end_date = LocalDate.of(2024, Month.APRIL, 1);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Developer getDeveloper() {
        return developer;
    }

    public void setDeveloper(Developer developer) {
        this.developer = developer;
    }

    public Label getLabel() {
        return label;
    }

    public void setLabel(Label label) {
        this.label = label;
    }

    public LocalDate getStart_date() {
        return start_date;
    }

    public void setStart_date(LocalDate start_date) {
        this.start_date = start_date;
    }

    public LocalDate getEstimated_end_date() {
        return estimated_end_date;
    }

    public void setEstimated_end_date(LocalDate estimated_end_date) {
        this.estimated_end_date = estimated_end_date;
    }
}
