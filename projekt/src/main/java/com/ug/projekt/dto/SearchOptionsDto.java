package com.ug.projekt.dto;

import java.time.LocalDate;

public class SearchOptionsDto {

    public static final Integer PAGE = 2;
    private String name;
    private Boolean isEnded;
    private LocalDate start_from_date;
    private LocalDate end_from_date;

    private LocalDate now;

    public SearchOptionsDto(String name, Boolean isEnded, LocalDate start_from_date, LocalDate end_from_date) {
        this.name = name;
        this.isEnded = isEnded;
        this.start_from_date = start_from_date;
        this.end_from_date = end_from_date;
        this.now = LocalDate.now();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getEnded() {
        return isEnded;
    }

    public void setEnded(Boolean ended) {
        isEnded = ended;
    }

    public LocalDate getStart_from_date() {
        return start_from_date;
    }

    public void setStart_from_date(LocalDate start_from_date) {
        this.start_from_date = start_from_date;
    }

    public LocalDate getEnd_from_date() {
        return end_from_date;
    }

    public void setEnd_from_date(LocalDate end_from_date) {
        this.end_from_date = end_from_date;
    }

    public LocalDate getNow() {
        return now;
    }

    public void setNow(LocalDate now) {
        this.now = now;
    }
}
