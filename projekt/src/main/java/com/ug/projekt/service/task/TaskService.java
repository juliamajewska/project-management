package com.ug.projekt.service.task;

import com.ug.projekt.dto.DeveloperAssignDto;
import com.ug.projekt.dto.ProjectTaskStats;
import com.ug.projekt.dto.TasksDto;

import java.util.List;

public interface TaskService {
    List<ProjectTaskStats> getProjectTaskStats(Long projectId);
    List<TasksDto> getAllTasks();

    TasksDto assignDeveloperToTask(DeveloperAssignDto developerAssignDto, Long taskId);
}
