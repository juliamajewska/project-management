package com.ug.projekt.dto;

import com.ug.projekt.domain.Label;
import com.ug.projekt.domain.enums.LabelType;

public record LabelDto(
        Long id,
        LabelType name
) {
    public LabelDto(LabelType labelType) {
        this(null, labelType);
    }
    public static LabelDto from(Label label){
        return new LabelDto(label.getId(), label.getName());
    }
}
