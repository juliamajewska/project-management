package com.ug.projekt.dto;

import com.ug.projekt.domain.Developer;


public record DeveloperDto(
        Long id,
        String name,
        String last_name
) {
    public DeveloperDto(String name, String last_name) {
        this(null, name, last_name);
    }

    public static DeveloperDto from(Developer developer) {
        return new DeveloperDto(
                developer.getId(),
                developer.getName(),
                developer.getLast_name()
                );
    }
}