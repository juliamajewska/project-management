package com.ug.projekt.controller;

import com.ug.projekt.dto.*;
import com.ug.projekt.service.project.ProjectServiceImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping(value = "/api/project")
public class ProjectController {

    private final ProjectServiceImpl projectService;

    public ProjectController(ProjectServiceImpl projectService) {
        this.projectService = projectService;
    }
    @GetMapping("")
    ResponseEntity<List<ProjectDto>> getAllProjects() {
        List<ProjectDto> projects = projectService.getAllProjects();
        return new ResponseEntity<>(projects, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    ResponseEntity<ProjectDto> getProjectById(@PathVariable("id") Long id) {
        ProjectDto project = projectService.getProjectById(id);
        return  new ResponseEntity<>(project, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    ResponseEntity<String> deleteProjectById(@PathVariable("id") Long id) {
        projectService.deleteProjectById(id);
        return new ResponseEntity<>("Project deleted", HttpStatus.NO_CONTENT);
    }

    @GetMapping("/stats")
    ResponseEntity<List<ProjectStats>> getStats() {
        List<ProjectStats> stats = projectService.getProjectStats();
        return new ResponseEntity<>(stats, HttpStatus.OK);
    }

    @GetMapping("/developer/{id}")
    ResponseEntity<List<ProjectInfo>> getDevelopersProjects(@PathVariable("id") Long id) {
        List<ProjectInfo> projects = projectService.findProjectsDeveloperIn(id);
        return new ResponseEntity<>(projects, HttpStatus.OK);
    }

    @PostMapping("")
    ResponseEntity<ProjectDto> addProject(@RequestBody AddProjectDto addProjectDto) {
        ProjectDto savedProject = projectService.addProject(addProjectDto);
        return new ResponseEntity<>(savedProject, HttpStatus.CREATED);
    }


    @GetMapping("/search")
    public List<ProjectDto> searchProject(
            @RequestParam(required = false) String name,
            @RequestParam(required = false) LocalDate startDate,
            @RequestParam(required = false) LocalDate endDate,
            @RequestParam(required = false) Boolean isEnded,
            @RequestParam(defaultValue = "0", required = false) Integer page,
            @RequestParam(required = false, defaultValue = "name") String sortOption,
            @RequestParam(required = false, defaultValue = "ASC") String sortType
    ) {

        SearchOptionsDto searchOptionsDto = new SearchOptionsDto(name, isEnded, startDate, endDate);
        SortOptionDto sortOptionDto = new SortOptionDto(sortOption, sortType);

        return projectService.searchProject(searchOptionsDto, page, sortOptionDto);
    }

    @PutMapping("/{projectId}")
    public ResponseEntity<ProjectDto> updateProject(@PathVariable Long projectId, @RequestBody ProjectUpdateDto projectUpdateDto) {
        ProjectDto updatedProjectDto = projectService.updateProject(projectId, projectUpdateDto);
        return new ResponseEntity<>(updatedProjectDto, HttpStatus.OK);
    }
}
