package com.ug.projekt.dto;

import com.ug.projekt.domain.Task;

import java.time.LocalDate;

public record TasksDto(
        Long id,
        String name,
        String description,

        LabelDto label,
        DeveloperDto developer,
        LocalDate start_date,
        LocalDate estimated_end_date

) {
    public TasksDto(String name, String description, LabelDto label, DeveloperDto developerDto, LocalDate start_date, LocalDate estimated_end_date) {
        this(null, name, description, label, developerDto, start_date, estimated_end_date);
    }

    public static TasksDto from(Task task){
        return new TasksDto(
                task.getId(),
                task.getName(),
                task.getDescription(),
                LabelDto.from(task.getLabel()),
                DeveloperDto.from(task.getDeveloper()),
                task.getStart_date(),
                task.getEstimated_end_date()
        );
    }
}
