package com.ug.projekt.dto;

import com.ug.projekt.domain.Project;

import java.time.LocalDate;
import java.util.List;

public record ProjectDto(
        Long id,
        String name,
        String description,
        LocalDate start_date,
        LocalDate end_date,
        List<TasksDto> taskList,
        List<DeveloperDto> developerList

) {

    public ProjectDto(String name, String description, LocalDate start_date, LocalDate end_date, List<TasksDto> taskDto, List<DeveloperDto> developerDto) {
        this(null, name, description, start_date, end_date, taskDto, developerDto);
    }

    public static ProjectDto from(Project project) {
        return new ProjectDto(
                project.getId(),
                project.getName(),
                project.getDescription(),
                project.getStart_date(),
                project.getEnd_date(),
                project.getTasksList().stream().map(TasksDto::from).toList(),
                project.getDeveloperList().stream().map(DeveloperDto::from).toList()
        );
    }
}
