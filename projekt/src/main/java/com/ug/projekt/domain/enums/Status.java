package com.ug.projekt.domain.enums;

public enum Status {
    TO_DO,
    IN_PROGRESS,
    QUALITY_ASSURANCE,
    DONE,
}
