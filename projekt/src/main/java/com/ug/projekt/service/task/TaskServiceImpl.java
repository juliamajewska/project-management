package com.ug.projekt.service.task;

import com.ug.projekt.domain.Developer;
import com.ug.projekt.domain.Task;
import com.ug.projekt.dto.*;
import com.ug.projekt.exceptions.NotFoundException;
import com.ug.projekt.repository.DeveloperRepository;
import com.ug.projekt.repository.ProjectRepository;
import com.ug.projekt.repository.TaskRepository;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskServiceImpl implements TaskService {

    private final TaskRepository taskRepository;
    private final ProjectRepository projectRepository;
    private final DeveloperRepository developerRepository;

    public TaskServiceImpl(TaskRepository taskRepository, ProjectRepository projectRepository, DeveloperRepository developerRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
        this.developerRepository = developerRepository;
    }

    public List<ProjectTaskStats> getProjectTaskStats(Long projectId) {
        projectRepository.findById(projectId).orElseThrow(() -> new NotFoundException("Project with provided id doesnt exists"));
        return taskRepository.getTaskStatsByProjectId(projectId);
    }

    public List<TasksDto> getAllTasks() {
        return taskRepository.findAll().stream().map(TasksDto::from).toList();
    }


    @Transactional
    public TasksDto assignDeveloperToTask(DeveloperAssignDto developerAssignDto, Long taskId) {
        Developer dev = developerRepository.findById(developerAssignDto.developerId()).orElseThrow(() -> new NotFoundException("Developer with provided id doesnt exist"));
        Task task = taskRepository.findById(taskId).orElseThrow(() -> new NotFoundException("Task with provided id doesnt exist"));
        task.setDeveloper(dev);
        Task updatedTask = taskRepository.save(task);
        return TasksDto.from(updatedTask);
    }
}
