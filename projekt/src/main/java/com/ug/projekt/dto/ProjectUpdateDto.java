package com.ug.projekt.dto;

import java.time.LocalDate;

public record ProjectUpdateDto(
        String name,
        String description,
        LocalDate endDate,
        LocalDate startDate
) {}
