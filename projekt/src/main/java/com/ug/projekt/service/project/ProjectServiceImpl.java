package com.ug.projekt.service.project;

import com.ug.projekt.domain.Developer;
import com.ug.projekt.domain.Project;
import com.ug.projekt.domain.Task;
import com.ug.projekt.dto.*;
import com.ug.projekt.exceptions.NotFoundException;
import com.ug.projekt.repository.DeveloperRepository;
import com.ug.projekt.repository.ProjectRepository;
import com.ug.projekt.repository.TaskRepository;
import jakarta.transaction.Transactional;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

@Service
@Transactional
public class ProjectServiceImpl implements ProjectService {

    private final ProjectRepository projectRepository;
    private final DeveloperRepository developerRepository;
    private final TaskRepository taskRepository;

    public ProjectServiceImpl(ProjectRepository projectRepository, DeveloperRepository developerRepository, TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.developerRepository = developerRepository;
        this.taskRepository = taskRepository;
    }

    public List<ProjectDto> getAllProjects() {
        return projectRepository.findAll().stream().map(ProjectDto::from).toList();
    }

    public ProjectDto getProjectById(Long id) {
        return ProjectDto.from(projectRepository.findById(id).orElseThrow(() -> new NotFoundException("Project not found")));
    }

    @Transactional
    public void deleteProjectById(Long id) {
        this.getProjectById(id);
        projectRepository.deleteById(id);
    }

    public List<ProjectStats> getProjectStats() {
        return projectRepository.getProjectStats();
    }

    public List<ProjectInfo> findProjectsDeveloperIn(Long id) {
        developerRepository.findById(id).orElseThrow(() -> new NotFoundException("Developer with provided id not found"));
        return projectRepository.findProjectsByDeveloperId(id);
    }

    @Transactional
    public ProjectDto addProject(AddProjectDto addProjectDto) {
        Project projectToAdd = Project.from(addProjectDto);
        List<Task> emptyTaskList = Collections.emptyList();
        List<Developer> emptyDeveloperList = Collections.emptyList();

        projectToAdd.setTasksList(emptyTaskList);
        projectToAdd.setDeveloperList(emptyDeveloperList);

        return ProjectDto.from(projectRepository.save(projectToAdd));
    }

    public List<ProjectDto> searchProject(SearchOptionsDto searchOptionsDto, Integer page, SortOptionDto sortOptionDto) {

        if (searchOptionsDto.getName() == null &&
                searchOptionsDto.getStart_from_date() == null &&
                searchOptionsDto.getEnd_from_date() == null &&
                searchOptionsDto.getEnded() == null) {
            return projectRepository.findAll(PageRequest.of(page, SearchOptionsDto.PAGE)).stream().map(ProjectDto::from).toList();
        }
        return projectRepository
                .searchProject(
                        PageRequest.of(page, SearchOptionsDto.PAGE,  Sort.by(sortOptionDto.getSortMode(), sortOptionDto.getSortField())),
                        searchOptionsDto.getName(),
                        searchOptionsDto.getStart_from_date(),
                        searchOptionsDto.getEnd_from_date(),
                        searchOptionsDto.getNow(),
                        searchOptionsDto.getEnded())
                            .stream()
                            .map(ProjectDto::from)
                            .toList();

    }

    @Transactional
    public ProjectDto updateProject(Long projectId, ProjectUpdateDto projectUpdateDto) {
        Project project = projectRepository.findById(projectId)
                .orElseThrow(() -> new NotFoundException("Project with provided id doesn't exist"));

        if (projectUpdateDto.name() != null) {
            project.setName(projectUpdateDto.name());
        }
        if (projectUpdateDto.description() != null) {
            project.setDescription(projectUpdateDto.description());
        }
        if (projectUpdateDto.endDate() != null) {
            LocalDate newEndDate = projectUpdateDto.endDate();
            if (newEndDate.isBefore(project.getStart_date()) || newEndDate.isEqual(project.getStart_date())) {
                throw new IllegalArgumentException("End date must be after the start date.");
            }
            List<Task> tasks = taskRepository.findByProject_Id(projectId);
            for (Task task : tasks) {
                if (task.getEstimated_end_date() == null || task.getEstimated_end_date().isAfter(newEndDate)) {
                    task.setEstimated_end_date(newEndDate);
                }
            }
            project.setEnd_date(newEndDate);
        }
        if (projectUpdateDto.startDate() != null) {
            project.setStart_date(projectUpdateDto.startDate());
        }

        Project updatedProject = projectRepository.save(project);
        return ProjectDto.from(updatedProject);
    }

}
