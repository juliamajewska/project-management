package com.ug.projekt.domain;

import jakarta.persistence.*;

import java.util.List;

@Entity
@Table
public class Developer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String last_name;

    @OneToMany(mappedBy = "developer", fetch = FetchType.LAZY)
    private List<Task> taskList;

    @ManyToMany(mappedBy = "developerList", fetch = FetchType.LAZY)
    private List<Project> projects;

    public Developer() {}
    public Developer(String name, String last_name) {
        this.name = name;
        this.last_name = last_name;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public List<Task> getTaskList() {
        return taskList;
    }

    public void setTaskList(List<Task> taskList) {
        this.taskList = taskList;
    }

    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }
}
