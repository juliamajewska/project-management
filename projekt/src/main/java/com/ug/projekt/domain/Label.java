package com.ug.projekt.domain;

import com.ug.projekt.domain.enums.LabelType;
import jakarta.persistence.*;

@Entity
@Table
public class Label {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    private LabelType name;

    public Label() {}

    public Label(LabelType name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LabelType getName() {
        return name;
    }

    public void setName(LabelType name) {
        this.name = name;
    }
}
