package com.ug.projekt.dto;
import org.springframework.data.domain.Sort;

public class SortOptionDto {
    private String sortField;
    private Sort.Direction sortMode;

    public  SortOptionDto(String sortField, String sortMode) {
        this.sortField = sortField;
        this.sortMode = "DESC".equalsIgnoreCase(sortMode) ? Sort.Direction.DESC : Sort.Direction.ASC;
    }

    public String getSortField() {
        return sortField;
    }

    public void setSortField(String sortField) {
        this.sortField = sortField;
    }

    public Sort.Direction getSortMode() {
        return sortMode;
    }

    public void setSortMode(Sort.Direction sortMode) {
        this.sortMode = sortMode;
    }
}