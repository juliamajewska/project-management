package com.ug.projekt.dto;

import java.time.LocalDate;

public interface ProjectInfo {
    String getName();
    String getDescription();
    LocalDate getStartDate();
    LocalDate getEndDate();
    Long getProjectId();
}
