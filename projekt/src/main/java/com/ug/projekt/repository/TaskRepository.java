package com.ug.projekt.repository;

import com.ug.projekt.domain.Task;
import com.ug.projekt.dto.ProjectTaskStats;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {

    @Query("SELECT t.label.name AS labelName, COUNT(t.id) AS taskCount " +
            "FROM Task t " +
            "WHERE t.project.id = :projectId " +
            "GROUP BY t.label.name")
    List<ProjectTaskStats> getTaskStatsByProjectId(@Param("projectId") Long projectId);

    List<Task> findByProject_Id(Long projectId);


}