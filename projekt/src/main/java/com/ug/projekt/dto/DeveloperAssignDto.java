package com.ug.projekt.dto;

public record DeveloperAssignDto(
        Long developerId
) {}
