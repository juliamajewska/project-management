package com.ug.projekt;

import com.ug.projekt.domain.Developer;
import com.ug.projekt.domain.Label;
import com.ug.projekt.domain.Project;
import com.ug.projekt.domain.Task;
import com.ug.projekt.domain.enums.LabelType;
import com.ug.projekt.repository.DeveloperRepository;
import com.ug.projekt.repository.LabelRepository;
import com.ug.projekt.repository.ProjectRepository;
import com.ug.projekt.repository.TaskRepository;
import jakarta.persistence.EntityManager;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;


@SpringBootApplication
public class ProjektApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjektApplication.class, args);
	}

	@Bean
	@Transactional
	public CommandLineRunner initializeData(ProjectRepository projectRepository, TaskRepository taskRepository, LabelRepository labelRepository, DeveloperRepository developerRepository){
		return args -> {
//			System.out.println("COMMANDLINERUNNER RUN");
//			Label labelFront = new Label(LabelType.FRONTEND);
//			Label labelBackend = new Label(LabelType.BACKEND);
//			labelRepository.saveAll(List.of(labelBackend, labelFront));
//
//			Developer developer1 = developerRepository.save(new Developer("Julia", "Majewska"));
//			Developer developer2 = developerRepository.save(new Developer("Test", "Testowy"));
//			Developer developer3 = developerRepository.save(new Developer("Julia", "Testowa"));
//
//			Project project = new Project("Project Name 1",
//					"Project Description 1",
//					LocalDate.of(2024, Month.FEBRUARY, 5),
//					LocalDate.of(2024, Month.FEBRUARY, 10),
//					List.of(developer1, developer2));
//			Project project2 = new Project("Project Name 2",
//					"Project Description 2",
//					LocalDate.of(2024, Month.FEBRUARY, 5),
//					LocalDate.of(2024, Month.FEBRUARY, 10),
//					List.of(developer1, developer3));
//			Project project3 = new Project("Project Name 3",
//					"Project Description 3",
//					LocalDate.of(2024, Month.FEBRUARY, 5),
//					LocalDate.of(2024, Month.FEBRUARY, 10),
//					List.of(developer2, developer3));
//
//			List<Project> projects = projectRepository.saveAll(List.of(project2, project3, project));
//
//			// Save tasks
//			Task task1 = new Task("Task 1", "Description 1", project, developer1, labelFront);
//			Task task2 = new Task("Task 2", "Description 2", project, developer2, labelFront);
//			Task task3 = new Task("Task 3", "Description 3", project2, developer1, labelBackend);
//			Task task4 = new Task("Task 4", "Description 4", project2, developer3, labelFront);
//			Task task5 = new Task("Task 5", "Description 5", project3, developer2, labelBackend);
//			Task task6 = new Task("Task 6", "Description 6", project3, developer3, labelFront);
//
//			List<Task> tasks = taskRepository.saveAll(List.of(task1, task2, task3, task4, task5, task6));
//
//			projects.get(0).setTasksList(List.of(tasks.get(0), tasks.get(1)));
//			projects.get(1).setTasksList(List.of(tasks.get(2), tasks.get(3)));
//			projects.get(2).setTasksList(List.of(tasks.get(4), tasks.get(5)));
//			projectRepository.saveAll(projects);

		};
	}
}
