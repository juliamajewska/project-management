package com.ug.projekt.dto;

public interface ProjectTaskStats {
    String getLabel();
    Long getTaskCount();

}
