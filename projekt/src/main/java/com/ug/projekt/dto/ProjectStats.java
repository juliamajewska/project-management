package com.ug.projekt.dto;

public interface ProjectStats {
    String getProjectId();
    String getProjectName();
    Long getNumberOfDevelopers();
    Long getNumberOfTasks();
}
