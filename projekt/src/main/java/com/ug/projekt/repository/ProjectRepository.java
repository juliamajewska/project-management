package com.ug.projekt.repository;

import com.ug.projekt.domain.Project;
import com.ug.projekt.dto.ProjectInfo;
import com.ug.projekt.dto.ProjectStats;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {

    @Query(value = "SELECT a.id AS projectId, a.name AS projectName, COUNT(c.id) AS numberOfTasks, COUNT(b.id) AS numberOfDevelopers " +
            "FROM Project a " +
            "LEFT JOIN a.developerList b " +
            "LEFT JOIN a.tasksList c " +
            "GROUP BY a.id")
    List<ProjectStats> getProjectStats();

    @Query(value = "SELECT p.id as projectId, p.name as name, p.description as description, p.start_date as startDate, p.end_date as endDate FROM Project p JOIN project_developer_list pdl ON p.id = pdl.projects_id WHERE pdl.developer_list_id = ?1", nativeQuery = true)
    List<ProjectInfo> findProjectsByDeveloperId(Long developerId);


    @Query("SELECT m FROM Project m WHERE (m.name LIKE %:name%) " +
            "AND (:startDate IS NULL OR m.start_date >= :startDate) " +
            "AND (:endDate IS NULL OR m.end_date <= :endDate) " +
            "AND (:isEnded IS NULL OR (:isEnded = true AND m.end_date <= :nowDate) OR (:isEnded = false AND m.end_date >= :nowDate))")
    List<Project> searchProject(Pageable pageable,
                             @Param("name") String name,
                             @Param("startDate") LocalDate startDate,
                             @Param("endDate") LocalDate endDate,
                             @Param("nowDate") LocalDate nowDate,
                             @Param("isEnded") Boolean isEnded);
}
