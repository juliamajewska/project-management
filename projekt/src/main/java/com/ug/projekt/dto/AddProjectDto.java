package com.ug.projekt.dto;

import java.time.LocalDate;

public record AddProjectDto(
        String name,
        String description,
        LocalDate start_date,
        LocalDate end_date
) {}
